const TelegramBot = require('node-telegram-bot-api');
const TelegramBotApi = require('node-telegram-bot-api');
const { Api, TelegramClient } = require('telegram');
const axios = require('axios');
const fs = require('fs');
const readXlsxFile = require('read-excel-file/node');

let settings;

const EXCEL_MIME_TYPE =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

const yandexApi =
  'https://cloud-api.yandex.net/v1/disk/public/resources/download?';

const Messages = {
  loadFromExcel: 'Загрузить данные из Excel файла',
  loadFromLink: 'Загрузить данные по ссылке Яндекс Диска',
  info: 'Задача',
  showSettings: 'Показать настройки',
  requirementsExcel: 'Требования к Excel',
  setStartSendInvites: 'Начать рассылку приглашений',
  setStopSendInvites: 'Закончить рассылку приглашений',
  setChannelToInvite: 'Установить канал для рассылки',
  setDailyInviteLimit: 'Установить дневной лимит рассылки',
  setInviteIntervalInMinutes: 'Установить интервал приглашений в минутах',
};

/**
 * Проверить номер телефона
 * @param {*} phone - string Номер телефона
 * @returns true / false Является ли номер телефона валидным
 */
const isPhoneNumber = phone =>
  /^((8|\+?7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(phone);

/**
 * Достать телефоны из экселя
 *
 * @param {string} filepath - string Путь до эксель файла
 * @returns массив номеров телефонов
 */
const getPhonesFromExcel = async filepath =>
  (await readXlsxFile(filepath))
    .map(r => r[0].toString())
    .filter(isPhoneNumber);

const tgBotToken = '5748607013:AAEwCmOtVCZ2_E2WcmKB2T6NrNEj4R3s1U4';
const bot = new TelegramBot(tgBotToken, { polling: true });
let inviteInterval = undefined;

/**
 * Попросить пользователя ввести данные
 * @param {string} message - Сообщение пользователю
 * @param {number} chatId - ID чата пользователя
 * @returns Ответ пользователя
 */
const sendMessageWithReply = async (message, chatId) => {
  return new Promise(async resolve => {
    const prompt = await bot.sendMessage(chatId, message, {
      reply_markup: {
        force_reply: true,
      },
    });

    bot.onReplyToMessage(chatId, prompt.message_id, async msg => {
      resolve(msg.text);
    });
  });
};

/** Загрузить настройки бота при старте */
if (fs.existsSync('settings.json')) {
  settings = JSON.parse(fs.readFileSync('settings.json'));
} else {
  settings = {
    phones: {
      name: 'База телефонов',
      value: [],
    },
    inviteIntervalInMinutes: {
      name: 'Интервал приглашений в минутах',
      value: 1,
    },
    dailyInviteLimit: {
      name: 'Дневной лимит',
      value: 200,
    },
    stringSession: {
      name: 'строка сессии',
      value: '',
    },
    channelToInvite: {
      name: 'Канал куда приглашать',
      value: '',
    },
    isSendsInvites: {
      name: 'Приглашения рассылаюся',
      value: false,
    },
  };
  fs.writeFileSync('settings.json', JSON.stringify(settings));
}
console.log(settings);

// client = new TelegramClient(
//   new StringSession(settings.stringSession.value),
//   this.config.apiId,
//   this.config.apiHash,
//   {
//     connectionRetries: 5,
//   },
// );

/**
 * Установить базу телефонов для рассылки
 * @param {string[]} phones - Массив строк телефонов
 */
const setTelephonesSettings = ({ phones }) => {
  settings.phones.value = phones;
  fs.writeFileSync('settings.json', JSON.stringify(settings));
};

/**
 * Установить дневной лимит для рассылки
 * @param {number} dailyInviteLimit - Дневной лимит
 */
const setDailyInviteLimitSettings = (dailyInviteLimit = 10) => {
  settings.dailyInviteLimit.value = dailyInviteLimit;
  fs.writeFileSync('settings.json', JSON.stringify(settings));
};

/**
 * Установить интервал в минутах для рассылки
 * @param {number} inviteIntervalInMinutes - интервал в минутах для рассылки
 */
const setInviteIntervalInMinutes = (inviteIntervalInMinutes = 10) => {
  settings.inviteIntervalInMinutes.value = inviteIntervalInMinutes;
  fs.writeFileSync('settings.json', JSON.stringify(settings));
};

/**
 * Установить флаг для рассылки приглашений
 * @param {boolean} isSendsInvites - Рассылать ли приглашения
 */
const setIsSendsInvites = (isSendsInvites = false) => {
  settings.isSendsInvites.value = isSendsInvites;
  fs.writeFileSync('settings.json', JSON.stringify(settings));
  if (inviteInterval) {
    clearInterval(inviteInterval);
  }
  if (isSendsInvites) {
    let phoneIndex = 0;
    inviteInterval = setInterval(async () => {
      console.log('Отправка инвайта пользователю с номерм = ${phoneToSend}');
      const phoneToSend = settings.phones.value[phoneIndex];

      phoneIndex++;

      phoneIndex = phoneIndex % settings.phones.value.length;
      if (!phoneToSend) {
        return;
      }

      try {
        // отправляем инвайт через клиентское апи
      } catch (err) {
        console.log(err);
      }
    }, +settings.inviteIntervalInMinutes.value * 1000 * 1);
  }
};

/**
 * Установить ссылку на тг-канал для рассылки приглашений
 * @param {string} channel  - ссылка на тг-канал
 */
const setChannelToInviteSettings = (channel = 'test') => {
  settings.channelToInvite.value = channel;
  fs.writeFileSync('settings.json', JSON.stringify(settings));
};

/**
 * Показать пользователю меню
 * @param {string} chatId - ID чатв пользователя
 */
const showMainMenu = async chatId => {
  await bot.sendMessage(chatId, 'Функции бота', {
    reply_markup: {
      keyboard: [
        [Messages.requirementsExcel],
        [Messages.info],
        [Messages.loadFromExcel],
        [Messages.showSettings],
        [
          settings.isSendsInvites.value
            ? Messages.setStopSendInvites
            : Messages.setStartSendInvites,
        ],
        [Messages.setChannelToInvite],
        [Messages.setDailyInviteLimit],
        [Messages.setInviteIntervalInMinutes],
      ],
    },
  });
};

bot.onText(/\/start/, msg => {
  showMainMenu(msg.chat.id);
});

bot.onText(new RegExp(Messages.loadFromExcel), msg => {
  bot.sendMessage(msg.chat.id, 'Прикрепите xlsx файл');
});

bot.onText(new RegExp(Messages.loadFromLink), msg => {
  bot.sendMessage(msg.chat.id, 'Отправьте ссылку на файл');
});

bot.onText(new RegExp(Messages.info), msg => {
  bot.sendMessage(
    msg.chat.id,
    `
  Разработать чат-бота в Telegram который позволяет по базе номеров в таблице находить пользователей и добавлять их в телеграм канал.

Что необходимо сделать?

 Получать номера телефонов из табличного документа и сохранять id пользователей, соответствующих номерам в документе. Добавлять контакты по id в канал. 
 Необходимо предусмотреть возможность работы бота с локальными файлами на компьютере пользователя, а так же с фалами доступными по ссылке через сервисы Google Docs и Яндекс.Документы. Пользователь должен иметь возможность регулировать скорость добавления контактов в канал, а так же устанавливать суточный лимит. Лимит добавлений не должен превышать 200 в сутки.

Требования к проекту и доступы

 Перед началом работы подготовить блок-схему работы чат-бота, отобразить зависимости, показать логику работы. Обосновать выбор стека разработки. Объяснить причину его выбора сравнить с другими возможными реализациями. Обосновать эффективность выбранного стека.  
 Разработку проекта и документирования вести в Gitlab. Обратить внимание на качество документации. Документация к коду должна быть обязательно и является неотъемлемой частью задачи.
 При разработке необходимо создать свой чат-бот в Telegram и решить поставленную задачу. После того как задача будет выполнена необходимо развернуть код на корпоративном тестовом VDS. 
 Работы с сервисами необходимыми для решения поставленной задачи должна вестись с использованием выданного логина на домене @t-code.ru. Адрес выданного ящика должен являться вашим логином.
  
  `,
  );
});

bot.onText(new RegExp(Messages.requirementsExcel), msg => {
  bot.sendMessage(
    msg.chat.id,
    `Данные должны располагаться в первой колонке \n Пример:`,
  );
  bot.sendPhoto(
    msg.chat.id,
    'https://sun9-13.userapi.com/impg/dDmbAJLfE19Jl17-VwkWSH8CsKbTQxo3yxIVcQ/3pNXFvaeLXg.jpg?size=172x182&quality=96&sign=f9516947dbd6c024d4b17fc7efd13d1c&type=album',
  );
});

bot.onText(new RegExp(Messages.showSettings), msg => {
  let settingsString = '';
  Object.values(settings).forEach(setting => {
    settingsString += `${setting.name}: ${JSON.stringify(setting.value)} \n`;
  });
  bot.sendMessage(msg.chat.id, settingsString);
});

bot.on('document', async msg => {
  if (msg.document.mime_type !== EXCEL_MIME_TYPE) {
    return bot.sendMessage(msg.chat.id, 'Это не Excel файл');
  }

  console.log(msg.document.file_id);
  const filePath = await bot.downloadFile(msg.document.file_id, './');

  const phones = await getPhonesFromExcel(filePath);

  bot.sendMessage(msg.chat.id, 'Полученные номера телефонов:');
  bot.sendMessage(msg.chat.id, phones.join('\n'));
  setTelephonesSettings({ phones });

  fs.unlinkSync(filePath);
});

bot.onText(/^https:\/\/disk\.yandex\.ru/, async msg => {
  bot.sendMessage(msg.chat.id, 'Загружаю...');

  const filePath = `./file_to_write_downloaded_excel.xlsx`;

  try {
    const publicUrl = msg.text;
    const linkDownload = `${yandexApi}public_key=${publicUrl}`;
    const { data } = await axios.get(linkDownload);
    const res = await axios({
      url: data.href,
      method: 'GET',
      responseType: 'arraybuffer',
    });
    if (res.headers['content-type'] !== EXCEL_MIME_TYPE) {
      throw new Error('Загружен не excel файл');
    }

    const fileStream = fs.createWriteStream(filePath);
    await new Promise((resolve, reject) => {
      fileStream.write(res.data, err => {
        if (err) reject();

        fileStream.close();
        resolve();
      });
    });
  } catch (e) {
    bot.sendMessage(msg.chat.id, 'Это не на excel файл');
    return;
  }

  console.log(filePath);
  const phones = await getPhonesFromExcel(filePath);
  bot.sendMessage(msg.chat.id, 'Полученные номера телефонов:');
  bot.sendMessage(msg.chat.id, phones.join('\n'));
  setTelephonesSettings({ phones });

  fs.unlinkSync(filePath);
});

bot.onText(new RegExp(Messages.setChannelToInvite), async msg => {
  const channel = await sendMessageWithReply(
    'Введите ссылку на канал (Вы должны состоять в канале и иметь права на приглашение):',
    msg.chat.id,
  );
  setChannelToInviteSettings(channel);

  await bot.sendMessage(
    msg.chat.id,
    'Канал-получатель рассылки успешно изменён',
  );
  showMainMenu(msg.chat.id);
});
bot.onText(new RegExp(Messages.setDailyInviteLimit), async msg => {
  let dailyInvites = 0;

  do {
    dailyInvites = await sendMessageWithReply(
      'Введите дневной лимит приглашений (число):',
      msg.chat.id,
    );
    dailyInvites = +dailyInvites;
  } while (isNaN(dailyInvites));

  setDailyInviteLimitSettings(dailyInvites);

  await bot.sendMessage(
    msg.chat.id,
    'Дневной лимит приглашений успешно изменён',
  );
  showMainMenu(msg.chat.id);
});

bot.onText(new RegExp(Messages.setInviteIntervalInMinutes), async msg => {
  let interval = 1;
  do {
    interval = await sendMessageWithReply(
      'Введите интервал рассылки (число):',
      msg.chat.id,
    );
    interval = +interval;
  } while (isNaN(interval));

  setInviteIntervalInMinutes(interval);

  await bot.sendMessage(msg.chat.id, 'Интервал рассылки успешно изменён');
  showMainMenu(msg.chat.id);
});

bot.onText(new RegExp(Messages.setStartSendInvites), async msg => {
  if (settings.phones.value.length === 0) {
    return bot.sendMessage(msg.chat.id, 'В базе данных нет номеров!');
  }
  setIsSendsInvites(true);
  await bot.sendMessage(msg.chat.id, `Рассылка начата`);
  showMainMenu(msg.chat.id);
});
bot.onText(new RegExp(Messages.setStopSendInvites), async msg => {
  setIsSendsInvites(false);
  await bot.sendMessage(msg.chat.id, `Рассылка остановлена`);
  showMainMenu(msg.chat.id);
});

console.log('TG BOT STARTED!');
